console.log("Hello Universe");

let getCube = 10**3;

let templateCube = `The Cube of 10 is ${getCube}`;
console.log(templateCube);

let address = ["Zone II-A", "Brgy. Libertad", "Surallah", "South Cotabto"];
let [barangay, subdivision, municipality, province] = address;
let home = `${barangay}, ${subdivision}, ${municipality}, ${province}`;
console.log(home);

let animal = {
	Name: "Lolong",
	Species: "Crocodile",
	Weight: 1075,
	Length: 20,
	inch: 3
}

let {Name, Species, Weight, Length, inch} = animal;

console.log(`${Name} was a saltwater ${Species}. He Weighed at ${Weight}kg with a measurement of ${Length}ft ${inch}in.`);

let numbers = [1,2,3,4,5];

numbers.forEach(numbers => console.log(numbers));

let reducedNumber = numbers.reduce((x,y) => x + y);
console.log(reducedNumber);

class dog {

	constructor(Name, Age, Breed) {

		this.Name = Name;
		this.Age = Age;
		this.Breed = Breed;
	}
}

let dog1 = new dog("Bakura", "4", "German Shepherd");
let dog2 = new dog("Kaiba", "5", "Bulldog");
let dog3 = new dog("Yugi", "6", "Poodle");

console.log(dog1);
console.log(dog2);
console.log(dog3);
